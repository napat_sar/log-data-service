package com.rattata.hackathon.commons.util;

import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * Created by NapatS on 8/26/2016.
 */
@Component
public class UUIDGenerator {
    public static String generate() {
        String uuid = UUID.randomUUID().toString();
        uuid = uuid.replace("-", "");
        return uuid;
    }
}

