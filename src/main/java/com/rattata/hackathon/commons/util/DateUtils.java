package com.rattata.hackathon.commons.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by NapatS on 8/26/2016.
 */
public class DateUtils {

    private static String format = "yyyy-MM-dd HH:mm:ss.SSS";

    public static Date toDate(String date) throws ParseException {
        DateFormat formatter = new SimpleDateFormat(format);
        return formatter.parse(date);
    }

    public static String fromDate(){
        return "";
    }
}
