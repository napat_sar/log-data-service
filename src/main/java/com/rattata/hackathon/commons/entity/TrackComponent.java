package com.rattata.hackathon.commons.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by NapatS on 8/26/2016.
 */
public class TrackComponent {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getStopTime() {
        return stopTime;
    }

    public void setStopTime(String stopTime) {
        this.stopTime = stopTime;
    }

    @JsonProperty("name")
    public String name;

    @JsonProperty("start_time")
    public String startTime;

    @JsonProperty("stop_time")
    public String stopTime;

}
