package com.rattata.hackathon.dataretriever.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

/**
 * Created by NapatS on 8/26/2016.
 */
@Entity
@Table(name = "Centralize_Transaction_Data")
public class CentralizeTransactionData {
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getStopTime() {
        return stopTime;
    }

    public void setStopTime(String stopTime) {
        this.stopTime = stopTime;
    }

    public Timestamp getReg_date() {
        return reg_date;
    }

    public void setReg_date(Timestamp reg_date) {
        this.reg_date = reg_date;
    }

    @Id
    @Column(name = "uuid")
    public String uuid;

    @Column(name = "transaction_id")
    public String transactionId;

    @Column(name = "component_name")
    public String componentName;

    @Column(name = "start_time")
    public String startTime;

    @Column(name = "stop_time")
    public String stopTime;

    @Column(name = "reg_date")
    public Timestamp reg_date;

}
