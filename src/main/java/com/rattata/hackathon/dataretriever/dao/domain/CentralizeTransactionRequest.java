package com.rattata.hackathon.dataretriever.dao.domain;

/**
 * Created by NapatS on 8/26/2016.
 */
public class CentralizeTransactionRequest {

    public String getTransactionId() {
        return TransactionId;
    }

    public void setTransactionId(String transactionId) {
        TransactionId = transactionId;
    }

    public String TransactionId;

}
