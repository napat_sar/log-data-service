package com.rattata.hackathon.dataretriever.dao;

import com.rattata.hackathon.dataretriever.dao.domain.CentralizeTransactionRequest;
import com.rattata.hackathon.dataretriever.dao.entity.CentralizeTransactionData;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by NapatS on 8/26/2016.
 */
@Repository
public class CentralizeDataDao {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    public List<CentralizeTransactionData> exchange(String requestTranId) {
        return  (List<CentralizeTransactionData>) entityManager.createQuery(
                "SELECT da FROM CentralizeTransactionData da WHERE da.transactionId = :transactionId ORDER BY da.startTime")
                .setParameter("transactionId", requestTranId)
                .getResultList();
    }

    public List<CentralizeTransactionData> fromdate(String date) {
        return  (List<CentralizeTransactionData>) entityManager.createQuery(
                "SELECT * FROM ctd.Centralize_Transaction_Data where (reg_date between '2016-08-26 11:15:29' and '2016-08-26 23:15:29')")
                .setParameter("transactionId", date)
                .getResultList();
    }

    @Transactional
    public void insert(CentralizeTransactionData data){
        entityManager.persist(data);
    }
}
