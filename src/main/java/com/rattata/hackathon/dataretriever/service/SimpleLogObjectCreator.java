package com.rattata.hackathon.dataretriever.service;

import com.rattata.hackathon.commons.entity.TrackComponent;
import com.rattata.hackathon.dataretriever.controller.domain.DataRetrieverResponse;
import com.rattata.hackathon.dataretriever.dao.entity.CentralizeTransactionData;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by NapatS on 8/26/2016.
 */
@Component
public class SimpleLogObjectCreator {

    public DataRetrieverResponse process(List<CentralizeTransactionData> list){
        DataRetrieverResponse response = new DataRetrieverResponse();
        String TranId = "";
        for(CentralizeTransactionData data: list){
            TrackComponent tc = new TrackComponent();
            if(TranId.isEmpty()){
                response.setTransactionId(data.getTransactionId());
            }
            tc.setStartTime(data.getStartTime());
            tc.setStopTime(data.getStopTime());
            tc.setName(data.getComponentName());
            response.add(tc);
        }
        return response;
    }
}
