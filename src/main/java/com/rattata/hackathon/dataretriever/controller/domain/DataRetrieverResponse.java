package com.rattata.hackathon.dataretriever.controller.domain;

/**
 * Created by NapatS on 8/26/2016.
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.rattata.hackathon.commons.entity.TrackComponent;

import java.util.ArrayList;
import java.util.List;

public class DataRetrieverResponse {

    public DataRetrieverResponse() {
        this.components = new ArrayList<TrackComponent>();
    }

    public List<TrackComponent> getComponents() {
        return components;
    }

    public void setComponents(List<TrackComponent> components) {
        this.components = components;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public void add(TrackComponent component) {
        this.components.add(component);
    }

    @JsonProperty("transaction_id")
    public String transactionId;

    @JsonProperty("component")
    public List<TrackComponent> components;

}
