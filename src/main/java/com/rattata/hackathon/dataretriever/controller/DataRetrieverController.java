package com.rattata.hackathon.dataretriever.controller;

import com.rattata.hackathon.commons.entity.TrackComponent;
import com.rattata.hackathon.commons.util.DateUtils;
import com.rattata.hackathon.commons.util.UUIDGenerator;
import com.rattata.hackathon.dataretriever.controller.domain.DataRetrieverResponse;
import com.rattata.hackathon.dataretriever.dao.CentralizeDataDao;
import com.rattata.hackathon.dataretriever.dao.entity.CentralizeTransactionData;
import com.rattata.hackathon.dataretriever.service.SimpleLogObjectCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by NapatS on 8/26/2016.
 */
@Controller
public class DataRetrieverController {

    @Autowired
    CentralizeDataDao centralizeDataDao;

    @Autowired
    SimpleLogObjectCreator sloc;

    @RequestMapping(path = "/adddata", method = RequestMethod.GET)
    public
    @ResponseBody
    String putData(@RequestParam(name = "tran_id", required = true) String tranId,
                   @RequestParam(name = "service_name", required = true) String serviceName,
                   @RequestParam(name = "start_time", required = true) String startTime,
                   @RequestParam(name = "end_time", required = true) String endTime) throws ParseException {
        CentralizeTransactionData data = new CentralizeTransactionData();
        data.setUuid(UUIDGenerator.generate());
        data.setTransactionId(tranId);
        data.setComponentName(serviceName);
        data.setStartTime(startTime);
        data.setStopTime(endTime);
        centralizeDataDao.insert(data);
        String response = "Success";
        return response;
    }

    @RequestMapping(path = "/getdata", method = RequestMethod.GET)
    public
    @ResponseBody
    DataRetrieverResponse getData(@RequestParam(name = "tran_id", required = true) String tranId) {
        List<CentralizeTransactionData> list = centralizeDataDao.exchange(tranId);
        DataRetrieverResponse response = sloc.process(list);
        return response;
    }

    @RequestMapping(path = "/getfromdate", method = RequestMethod.GET)
    public
    @ResponseBody
    DataRetrieverResponse getFromData(@RequestParam(name = "date", required = true) String date) {
        List<CentralizeTransactionData> list = centralizeDataDao.exchange(date);
        DataRetrieverResponse response = sloc.process(list);
        return response;
    }

    @RequestMapping(path = "/getmockdata", method = RequestMethod.GET)
    public
    @ResponseBody
    DataRetrieverResponse getMockData(@RequestParam(name = "tran_id", required = false) String tranId) {
        DataRetrieverResponse response = new DataRetrieverResponse();
        response.setTransactionId("123");
        //create mock data component
        List components = new ArrayList<TrackComponent>();
        TrackComponent componentA = new TrackComponent();
        componentA.setName("bps");
        componentA.setStartTime("26/08/2016 09:00:00.000");
        componentA.setStopTime("26/08/2016 09:00:05.374");
        TrackComponent componentB = new TrackComponent();
        componentB.setName("ads");
        componentB.setStartTime("26/08/2016 09:00:00.654");
        componentB.setStopTime("26/08/2016 09:00:02.374");
        TrackComponent componentC = new TrackComponent();
        componentC.setName("ctl");
        componentC.setStartTime("26/08/2016 09:00:01.130");
        componentC.setStopTime("26/08/2016 09:00:02.281");

        components.add(componentA);
        components.add(componentB);
        components.add(componentC);

        response.setComponents(components);
        return response;
    }

}
