DROP TABLE IF EXISTS Centralize_Transaction_Data;

CREATE TABLE Centralize_Transaction_Data (
	uuid VARCHAR(55) NOT NULL PRIMARY KEY,
	transaction_id VARCHAR(30) NOT NULL,
	component_name VARCHAR(30) NOT NULL,
	start_time VARCHAR(30) NOT NULL,
	stop_time VARCHAR(30) NOT NULL,
	reg_date TIMESTAMP
)